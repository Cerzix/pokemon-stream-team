package PokemonStreamTeam;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.stream.JsonReader;
 
/**
 * @author Crunchify.com 
 * Best and simple Production ready utility to save/load
 *         (read/write) data from/to file
 */
 
public class CrunchifyReadWriteUtilityForFile {
 
	
	private static Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
 
	// CrunchifyComapny Class with two fields
	// - Employees
	// - CompanyName
	public static class CrunchifyStreamTeam {
		@Expose
		private String[] pokemon;
		@Expose
		private boolean[] shiny;
		@Expose
		private String filePath;
 
		//Getter
		
		public String[] getPokemon() {
			return pokemon;
		}
		
		public boolean[] getShiny() {
			return shiny;
		}
 
		
		public String getFilePath() {
			return filePath;
		}
 
		//Setter
 
		public void SetPokemon(String[] _pokemon) {
			this.pokemon = _pokemon;
		}
 
		public void SetShiny(boolean[] _shiny) {
			this.shiny = _shiny;
		}
 
		public void SetFilePath(String _filePath) {
			this.filePath = _filePath;
		}
 
	}
 	
	@SuppressWarnings("rawtypes")
	public static void SaveData(JComboBox[] pokemon, JCheckBox[] shiny, String filePath, String savePath) {
		
		String[] pokemonContent = new String[6];
		boolean[] shinyContent = new boolean[6];
		
		int i = 0;
		for(JComboBox jcb : pokemon) {
			pokemonContent[i] = String.valueOf(jcb.getSelectedItem());
			i++;
		}
		
		i = 0;
		for(JCheckBox jcb : shiny) {
			shinyContent[i] = jcb.isSelected();
			i++;
		}
		
		CrunchifyStreamTeam crunchify = new CrunchifyStreamTeam();
		crunchify.SetPokemon(pokemonContent);
		crunchify.SetShiny(shinyContent);
		crunchify.SetFilePath(filePath);
		
		// Save data to file
		crunchifyWriteToFile(gson.toJson(crunchify), savePath);
	}
	
	public static CrunchifyStreamTeam LoadData(String loadPath) {
		CrunchifyStreamTeam retrievedTeam = new CrunchifyStreamTeam();
		CrunchifyStreamTeam crunchify = new CrunchifyStreamTeam();
 		
		// Retrieve data from file
		retrievedTeam = crunchifyReadFromFile(loadPath);
		
		crunchify.SetPokemon(retrievedTeam.getPokemon());
		crunchify.SetShiny(retrievedTeam.getShiny());
		crunchify.SetFilePath(retrievedTeam.getFilePath());
		
		return crunchify;
		}
 
	// Save to file Utility
	private static void crunchifyWriteToFile(String myData, String savePath) {
		File doesFileExist = new File(savePath);
		File crunchifyFile = new File("");
		
		if(!doesFileExist.exists()) {
			crunchifyFile = new File(savePath + ".team");
				try {
					File directory = new File(crunchifyFile.getParent());
					if (!directory.exists()) {
						directory.mkdirs();
					}
					crunchifyFile.createNewFile();
				} catch (IOException e) {
					log("Excepton Occured: " + e.toString());
				}
		}
		else {
			crunchifyFile = new File(savePath);
		}
 
		try {
			// Convenience class for writing character files
			FileWriter crunchifyWriter;
			crunchifyWriter = new FileWriter(crunchifyFile.getAbsoluteFile(), true);
 
			// Writes text to a character-output stream
			BufferedWriter bufferWriter = new BufferedWriter(crunchifyWriter);
			bufferWriter.write(myData.toString());
			bufferWriter.close();
 
			log("Saved at file location: " + crunchifyFile.getPath() + " Data: " + myData + "\n");
		} catch (IOException e) {
			log("Hmm.. Got an error while saving data to file " + e.toString());
		}
	}
 
	// Read From File Utility
	public static CrunchifyStreamTeam crunchifyReadFromFile(String loadPath) {
		File crunchifyFile = new File(loadPath);
		CrunchifyStreamTeam crunchify = new CrunchifyStreamTeam();
		
		if (!crunchifyFile.exists())
			log("File doesn't exist");
 
		InputStreamReader isReader;
		try {
			isReader = new InputStreamReader(new FileInputStream(crunchifyFile), "UTF-8");
 
			JsonReader myReader = new JsonReader(isReader);
						
			crunchify = gson.fromJson(myReader, CrunchifyStreamTeam.class);
			 
			return crunchify;
			
		} catch (Exception e) {
			log("error load cache from file " + e.toString());
		}
 
		log("\nComapny Data loaded successfully from file " + loadPath);
		return crunchify;
 
	}
 
	private static void log(String string) {
		System.out.println(string);
	}
	 
}