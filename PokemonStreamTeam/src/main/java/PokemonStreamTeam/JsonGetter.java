package PokemonStreamTeam;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject; 

public class JsonGetter {
	public static void main(String[] args) {
	}

	public static JSONObject GetRequest(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        System.out.println("\nSending 'GET' request to URL : " + url);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
        	System.out.println(inputLine);
        	response.append(inputLine);
        }
        in.close();
        
        //print in String
        System.out.println(response.toString());
        
        //Read JSON response and print
        JSONObject myResponse = new JSONObject(response.toString());
        System.out.println("result after Reading JSON Response");
        System.out.println(myResponse.toString());
        
        return myResponse;
	}
}
